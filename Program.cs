﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car1 = new Car();
            car1.Make = "Oldsmobile";
            car1.Model = "Cutlas Supreme";
            //Console.WriteLine(car1.ToString());

            Car car2 = new Car();
            car2.Make = "Geo";
            car2.Model = "Prism";

            Car car3 = new Car();
            car3.Make = "Kia";
            car3.Model = "Some";
            //Console.WriteLine(car2.ToString());

            Book b1 = new Book();
            b1.Author = "Robert Tabor";
            b1.Title = "Microsoft .NET XML Web Services";
            b1.ISBN = "0-000-00000-0";
            //Console.WriteLine(b1.ToString());

            Book b2 = new Book();
            b2.Author = "Sing Graajae";
            b2.Title = "Microsoft .NET F#";
            b2.ISBN = "0-000-00002-1";
            
            Book b3 = new Book();
            b3.Author = "Hello World";
            b3.Title = "Microsoft .NET C++";
            b3.ISBN = "0-000-02003-1";
            //Console.WriteLine(b1.ToString());

            // ArrayList
            Console.WriteLine("---ArrayList----");
            ArrayList carList = new ArrayList();
            carList.Add(car1);
            carList.Add(car2);
            carList.Add(car3);
            PrintArrayList(carList);

            ArrayList bookList = new ArrayList();
            bookList.Add(b1);
            bookList.Add(b2);
            bookList.Add(b3);
            PrintArrayList(bookList);

            // SortedDictionary
            Console.WriteLine("---SortedDictionary----");
            SortedDictionary<int, object> sortedDicCar = new SortedDictionary<int, object>();
            sortedDicCar.Add(0, car1);
            sortedDicCar.Add(2, car3);
            sortedDicCar.Add(1, car2);
            PrintSoredDic(sortedDicCar);

            SortedDictionary<int, object> sortedDicBook = new SortedDictionary<int,object>();
            sortedDicBook.Add(0, b1);
            sortedDicBook.Add(11, b3);
            sortedDicBook.Add(1, b2);
            PrintSoredDic(sortedDicBook);

            // SortedList
            Console.WriteLine("---SortedList----");
            SortedList<int, object> sortedListCar = new SortedList<int, object>();
            sortedListCar.Add(0, car1);
            sortedListCar.Add(2, car3);
            sortedListCar.Add(1, car2);
            PrintSoredList(sortedListCar);

            SortedList<int, object> sortedListBook = new SortedList<int, object>();
            sortedListBook.Add(0, b1);
            sortedListBook.Add(11, b3);
            sortedListBook.Add(1, b2);
            PrintSoredList(sortedListBook);

            // ListDictionary
            Console.WriteLine("---ListDictionary----");
            ListDictionary listDicCar = new ListDictionary();
            listDicCar.Add(0, car1);
            listDicCar.Add(2, car3);
            listDicCar.Add(1, car2);
            PrintListDic(listDicCar);

            ListDictionary listDicBook = new ListDictionary();
            listDicBook.Add(0, b1);
            listDicBook.Add(11, b3);
            listDicBook.Add(1, b2);
            PrintListDic(listDicBook);

            // List
            Console.WriteLine("---List----");
            List<object> listCar = new List<object>();
            listCar.Add(car1);
            listCar.Add(car3);
            listCar.Add(car2);
            PrintList(listCar);

            List<object> listBook = new List<object>();
            listBook.Add(b1);
            listBook.Add(b3);
            listBook.Add(b2);
            PrintList(listBook);

            Console.ReadLine();
        }

        private static void PrintArrayList(ArrayList arrList)
        {
            foreach(object obj in arrList)
            {
                Console.WriteLine(obj.ToString());
            }
        }

        private static void PrintSoredDic(SortedDictionary<int,object> dic)
        {
            foreach(KeyValuePair<int,object> kvp in dic)
            {
                Console.WriteLine(kvp.Key + ". " + kvp.Value.ToString());
            }
        }


        private static void PrintSoredList(SortedList<int,object> sList)
        {
            foreach(KeyValuePair<int,object> kvp in sList)
            {
                Console.WriteLine(kvp.Key + ". " +kvp.Value.ToString());
            }
        }

        private static void PrintListDic(ListDictionary listDic)
        {
            foreach(DictionaryEntry item in listDic)
            {
                Console.WriteLine(item.Key + ". " + item.Value.ToString());
            }
        }

        private static void PrintList(List<Object> list)
        {
            foreach(Object obj in list)
            {
                Console.WriteLine(obj.ToString());
            }
        }
    }

    

    class Car
    {
        public string Make { get; set; }
        public string Model { get; set; }
        public override string ToString()
        {
            return Make + "   " + Model;
        }
    }


    class Book
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string ISBN { get; set; }

        public override string ToString()
        {
            return Title + "   " + Author + "  " + ISBN;
        }
    }

}
